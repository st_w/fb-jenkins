﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using System.Net;
using System.IO;

namespace GetChangelog
{
    class Program
    {
        static void Main(string[] args)
        {

            String job = "FreeBasic Compiler MinGW";
            if (args.Length > 0) job = args[0];
            String uri = "http://office.mv-lacken.at:8080/jenkins/job/" + Uri.EscapeDataString(job) + "/changes";

            WebClient wc = new WebClient();
            String webData = "";
            try
            {
                wc.Encoding = Encoding.UTF8;
                webData = wc.DownloadString(uri);
            }
            catch (WebException) { return; }

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(webData);


            if (doc.DocumentNode != null)
            {
                HtmlNode chDoc = doc.DocumentNode.SelectSingleNode("//*[@id='main-panel']");

#if DEBUG
                String raw = "<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>Changelog " + job + "</title></head><body>"
                    + chDoc.WriteContentTo().Replace("Änderungen", "Changelog " + job) + "</body></html>";
                File.WriteAllText("result_raw.html", raw, Encoding.UTF8);
#endif

                foreach (HtmlNode link in chDoc.SelectNodes(".//a[@href]"))
                {
                    HtmlAttribute att = link.Attributes["href"];
                    //if (!att.Value.Contains("github")) att.Value = "#";

                    if (att.Value.Contains("user/")) { while (link.NextSibling != null) link.NextSibling.Remove(); }
                    //if (att.Value.Contains("changes#detail")) link.Remove();

                    if (!att.Value.Contains("github") && link.ParentNode != null)
                    {
                        if (link.HasChildNodes)
                        {
                            link.ParentNode.ReplaceChild(link.ChildNodes[0], link);
                        }
                        else
                        {
                            link.Remove();
                        }
                    }
                }

                String outp = "<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>Changelog " + job + "</title></head><body>"
                    + chDoc.WriteContentTo().Replace("Änderungen", "Changelog " + job) + "</body></html>";

#if DEBUG
                File.WriteAllText("result.html", outp, Encoding.UTF8);
#endif
                Console.OutputEncoding = Encoding.UTF8;
                Console.Write(outp);
           }
        }
    }
}
