# This file automatically generated - DO NOT EDIT
#
SRCLIST_COMPILE_ONLY_OK += ./qb/dim-common-dynamic-1.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/dim-common-dynamic-2.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/file_open_com.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/file_open_lpt.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/literal_types.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/redim-common-dynamic-1.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/redim-common-dynamic-2.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/redim-deftype-vs-astype-different-1.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/redim-deftype-vs-astype-different-2.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/redim-deftype-vs-astype-same-1.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/redim-deftype-vs-astype-same-2.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/redim-deftype-vs-suffix-different-1.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/redim-deftype-vs-suffix-different-2.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/redim-deftype-vs-suffix-same-1.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/redim-deftype-vs-suffix-same-2.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/redim-suffix-vs-astype-different-1.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/redim-suffix-vs-astype-different-2.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/redim-suffix-vs-astype-same-1.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/redim-suffix-vs-astype-same-2.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/suffixes.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/type-default-common.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/type-default-dim-shared.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/type-default-dim.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/type-default-redim-shared.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/type-default-redim.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/type-defint-redim.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/type-suffix-common-2.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/type-suffix-common.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/type-suffix-dim-shared.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/type-suffix-dim.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/type-suffix-redim-shared.bas
SRCLIST_COMPILE_ONLY_OK += ./qb/type-suffix-redim.bas
#
SRCLIST_COMPILE_ONLY_FAIL += ./qb/dupdef-dim-common-dynamic.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/dupdef-dim-dynamic-1.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/dupdef-dim-dynamic-2.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/dupdef-dim-dynamic-3.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/dupdef-redim-common-dynamic.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/dupdef-redim-deftype-1.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/dupdef-redim-deftype-2.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/dupdef-redim-deftype-3.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/dupdef-redim-suffix-1.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/dupdef-redim-suffix-2.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/dupdef-redim-suffix-3.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/extern-with-subscripts-option-dynamic-1.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/extern-with-subscripts-option-dynamic-2.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/file_open_com_fail.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/file_open_cons_fail.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/file_open_lpt_fail.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/file_open_scrn_fail.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/len-refuses-types.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/scope_block.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/select-code-before-case-1.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/select-code-before-case-2.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/type-suffixtype-vs-different-astype-redim.bas
SRCLIST_COMPILE_ONLY_FAIL += ./qb/type-suffixtype-vs-matching-astype-redim.bas
#
SRCLIST_COMPILE_AND_RUN_OK += ./qb/align_qb.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/call.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/data.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/def.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/extern-option-dynamic.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/file_open.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/file_open_cons.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/file_open_scrn.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/literal_sizes.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/local-suffixvar-overrides-shared-suffixvar.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/mkcv.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/offsetof.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/qbtypes.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/redim-staticlocals-1.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/redim-staticlocals-2.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/redim-suffix.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/rnd.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/static-and-option-dynamic.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/str.bas
SRCLIST_COMPILE_AND_RUN_OK += ./qb/suffixes2.bas
#
#
#
#
#
