
#Ifdef __FB_UNIX__
	#Define DIR_SEP "/"
#Else
	#Define DIR_SEP "\"
#EndIf

#Ifdef __FB_UNIX__
	#Define NL !"\n"
#Else
	#Define NL !"\r\n"
#EndIf


Type LogTest
	Test As String
	Package As String
	Mode As String
	Outp As String
	Result As Integer	= -1	'0=SUCCESS, otherwise FAIL; -1=SKIPPED
End Type


Dim Shared logTests(0 To 5000) As LogTest
Dim Shared logTestCnt As Integer



Function xmlEscape(d As String) As String
	
	Dim o As String
	Dim copied As Integer = 0
	Dim p As Integer = 0
	Do
		p = InStr(p+1, d, Any """'<>&")
		If p <= 0 Then Exit Do
		
		o += Mid(d, copied+1, p-copied-1)
		Select Case Mid(d, p, 1)
			Case """": o += "&quot;"
			Case "'": o += "&apos;"
			Case "<": o += "&lt;"
			Case ">": o += "&gt;"
			Case "&": o += "&amp;"
		End Select
		
		copied = p
	Loop
	If copied < Len(d) Then o += Mid(d, copied+1)
	
	Return o
End Function



Function readTests(filename As String) As Integer
	
	If Dir(filename) = "" Then Return 1

	Dim ff As Integer = FreeFile
	Open filename For Input As #ff
	
	Dim ln As String
	
	Do Until Eof(ff)
		Line Input #ff, ln
		ln = Trim(ln)
		If Mid(ln,1,1) = "#" Then Continue Do
		Dim listname As String
		Dim logfile As String
		Dim package As String
		'parse assignment string
		'e.g.: SRCLIST_MULTI_MODULE_OK += ./pretest/multi_module_ok.bmk
		Dim p As Integer = InStr(ln, "+=")	'split listname += logfile
		listname = Trim(Left(ln, p-1))
		logfile = Trim(Mid(ln, p+2))
		If Left(logfile, 2) = "./" Then logfile = Mid(logfile, 3)
		p = InStr(logfile, "/")			'split package/logfile
		package = Left(logfile, p-1)
		logfile = Mid(logfile, p+1)
		p = InStrRev(logfile, ".")		'remove file extension
		logfile = Left(logfile, p-1)
		
		If listname = "SRCLIST_CUNIT" Then Continue Do
		
		logTests(logTestCnt).Test = logfile
		logTests(logTestCnt).Mode = Mid(listname, 9)
		logTests(logTestCnt).Package = package
		logTestCnt += 1
		
	Loop
	
	Close #ff
	Return 0
	
End Function



Function readLogfile(test As LogTest Ptr) As Integer
	
	If Dir(test->Package + DIR_SEP + test->Test + ".log") = "" Then Return 1
	
	Dim ff As Integer = FreeFile
	Open test->Package + DIR_SEP + test->Test + ".log" For Binary As #ff
	Dim content As String = Space(Lof(ff))
	Get #ff,,content
	Close #ff
	
	If InStrRev(content, " RESULT=PASSED") > 0 Then test->Result = 0 Else test->Result = 1
	test->Outp = content
	
	Return 0
End Function



Function writeTestlog(filename As String) As Integer
	
	Dim c As String
	c = "<?xml version=""1.0"" encoding=""UTF-8""?>" + NL
	c += "<testsuites>" + NL
	
	For q As Integer = 0 To logTestCnt-1
		Dim t As LogTest Ptr = @logTests(q)
		
		c += "<testsuite name=""fbc_tests." + xmlEscape(t->Package + "." + t->Test) + """>" + NL
		
		c += "<testcase classname=""fbc_tests." + xmlEscape(t->Package + "." + t->Test) + """ name=""" + xmlEscape(t->Mode) + """ time=""0"""
		If t->Result = 0 Then
			c += ">" + NL
			c += "<system-out>"
			c += xmlEscape(t->Outp)
			c += "</system-out>" + NL
			c += "</testcase>" + NL
		Else
			c += ">" + NL
			c += "<failure message="""" type=""Failure"">" + NL
			c += xmlEscape(t->Outp)
         c += "</failure>" + NL
			c += "</testcase>" + NL
		EndIf
		
		c += "</testsuite>" + NL
		
	Next
	c += "</testsuites>" + NL
	
	If Dir(filename) <> "" Then Kill filename
	Dim ff As Integer = FreeFile
	Open filename For Binary As #ff
	Put #ff,,c
	
	Return 0
End Function







readTests("log-tests-fb.inc")
readTests("log-tests-deprecated.inc")
readTests("log-tests-qb.inc")

For q As Integer = 0 To logTestCnt-1
	readLogfile(@logTests(q))	
Next

writeTestlog("log-tests-Results.xml")






