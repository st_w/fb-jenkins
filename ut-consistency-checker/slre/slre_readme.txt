

SLRE: Super Light Regular Expression library
=============================================


this is the last version licensed under MIT License
from:
https://code.google.com/archive/p/slre/


newer versions are GPLv2 licensed and can be found here:
https://github.com/cesanta/slre



build static library:

gcc -c -o slre.o slre.c
ar rcs libslre.a slre.o

