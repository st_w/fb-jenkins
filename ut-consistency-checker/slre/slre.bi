
#LibPath __PATH__
#Inclib "slre"

/'
// This is a regular expression library that implements a subset of Perl RE.
// Please refer to http://slre.googlecode.com for detailed description.
//
// Supported syntax:
//    ^        Match beginning of a buffer
//    $        Match end of a buffer
//    ()       Grouping and substring capturing
//    [...]    Match any character from set
//    [^...]   Match any character but ones from set
//    \s       Match whitespace
//    \S       Match non-whitespace
//    \d       Match decimal digit
//    \r       Match carriage return
//    \n       Match newline
//    +        Match one or more times (greedy)
//    +?       Match one or more times (non-greedy)
//    *        Match zero or more times (greedy)
//    *?       Match zero or more times (non-greedy)
//    ?        Match zero or once
//    \xDD     Match byte with hex value 0xDD
//    \meta    Match one of the meta character: ^$().[*+\?
//    x|y      Match x or y (alternation operator)

// Match string buffer "buf" of length "buf_len" against "regexp", which should
// conform the syntax outlined above. "options" could be either 0 or
// SLRE_CASE_INSENSITIVE for case-insensitive match. If regular expression
// "regexp" contains brackets, slre_match() will capture the respective
// substring into the passed placeholder. Thus, each opening parenthesis
// should correspond to three arguments passed:
//   placeholder_type, placeholder_size, placeholder_address
//
// Usage example: parsing HTTP request line.
//
//  char method[10], uri[100];
//  int http_version_minor, http_version_major;
//  const char *error;
//  const char *request = " \tGET /index.html HTTP/1.0\r\n\r\n";

//  error = slre_match(0, "^\\s*(GET|POST)\\s+(\\S+)\\s+HTTP/(\\d)\\.(\\d)",
//                     request, strlen(request),
//                     SLRE_STRING,  sizeof(method), method,
//                     SLRE_STRING, sizeof(uri), uri,
//                     SLRE_INT,sizeof(http_version_major),&http_version_major,
//                     SLRE_INT,sizeof(http_version_minor),&http_version_minor);
//
//  if (error != NULL) {
//    printf("Error parsing HTTP request: %s\n", error);
//  } else {
//    printf("Requested URI: %s\n", uri);
//  }
//
//
// Return:
//   NULL: string matched and all captures successfully made
//   non-NULL: in this case, the return value is an error string


enum slre_option {SLRE_CASE_INSENSITIVE = 1};
enum slre_capture {SLRE_STRING, SLRE_INT, SLRE_FLOAT};

const char *slre_match(enum slre_option options, const char *regexp,
                       const char *buf, int buf_len, ...);
'/

Const As Long SLRE_CASE_INSENSITIVE = 1
Const As Long SLRE_STRING = 0
Const As Long SLRE_INT = 1
Const As Long SLRE_FLOAT = 2

Declare Function slre_match Cdecl Alias "slre_match" (options As Integer, regexp As ZString Ptr, buf As ZString Ptr, buf_len As Long, ...) As Integer
