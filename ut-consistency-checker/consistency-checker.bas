
#Include "slre/slre.bi"

#Ifdef __FB_UNIX__
	#Define DIR_SEP "/"
#Else
	#Define DIR_SEP "\"
#EndIf

#Ifdef __FB_UNIX__
	#Define NL !"\n"
#Else
	#Define NL !"\r\n"
#EndIf


Type LogTest
	Test As String
	Package As String
End Type


Dim Shared logTests(0 To 5000) As LogTest
Dim Shared logTestCnt As Integer




Function readTests(filename As String) As Integer
	
	If Dir(filename) = "" Then Return 1

	Dim ff As Integer = FreeFile
	Open filename For Input As #ff
	
	Dim ln As String
	
	Do Until Eof(ff)
		Line Input #ff, ln
		ln = Trim(ln)
		If Mid(ln,1,1) = "#" Then Continue Do
		Dim logfile As String
		Dim package As String
		'parse assignment string
		'e.g.: SRCLIST += ./numbers/const_op.bas
		Dim p As Integer = InStr(ln, "+=")	'split listname += logfile
		logfile = Trim(Mid(ln, p+2))
		If Left(logfile, 2) = "./" Then logfile = Mid(logfile, 3)
		p = InStr(logfile, "/")			'split package/logfile
		package = Left(logfile, p-1)
		logfile = Mid(logfile, p+1)
		p = InStrRev(logfile, ".")		'remove file extension
		logfile = Left(logfile, p-1)
		
		logTests(logTestCnt).Test = logfile
		logTests(logTestCnt).Package = package
		logTestCnt += 1
		
	Loop
	
	Close #ff
	Return 0
	
End Function



Function checkTest(test As LogTest Ptr) As Integer
	
	If Dir("." + DIR_SEP + test->Package + DIR_SEP + test->Test + ".bas") = "" Then Return 1
	
	Dim ff As Integer = FreeFile
	Open "." + DIR_SEP + test->Package + DIR_SEP + test->Test + ".bas" For Binary As #ff
	Dim content As String = Space(Lof(ff))
	Get #ff,,content
	Close #ff
	
	Dim result As Integer
	Dim suitename As ZString*100
	result = slre_match(SLRE_CASE_INSENSITIVE, "add_suite\s*\(\s*""([^""]+)""", content, Len(content), SLRE_STRING, SizeOf(suitename), suitename)
	If result <> 0 Then Return 1
	
	Dim As String suitename_expected = "fbc_tests." + test->Package + "." + test->Test
	
	Print
	Print "File: " + test->Package + DIR_SEP + test->Test + ".bas"
	Print "Actual SuiteName:   " + suitename
	If suitename_expected <> suitename Then
		Print "Expected SuiteName: " + suitename_expected
		Dim As Integer p 
		p = InStr(content, "add_suite")
		p = InStr(p, content, suitename)
		If p > 0 Then
			content = Left(content, p-1) + suitename_expected + Mid(content, p+Len(suitename))
			ff = FreeFile
			Kill "." + DIR_SEP + test->Package + DIR_SEP + test->Test + ".bas"
			Open "." + DIR_SEP + test->Package + DIR_SEP + test->Test + ".bas" For Binary As #ff
			Put #ff,,content
			Close #ff
		EndIf
		
	EndIf
	
	Dim test_ns As ZString*100
	result = slre_match(SLRE_CASE_INSENSITIVE, "\n\s*namespace\s+([^\s\n]+)\s*\n", content, Len(content), SLRE_STRING, SizeOf(test_ns), test_ns)
	If result <> 0 Then Return 1
	
	Print "Actual Namespace:   " + test_ns
	
	Return 0
End Function



Function writeTestlog(filename As String) As Integer
	/'
	Dim c As String
	c = "<?xml version=""1.0"" encoding=""UTF-8""?>" + NL
	c += "<testsuites>" + NL
	
	Dim curPackage As String
	For q As Integer = 0 To logTestCnt-1
		Dim t As LogTest Ptr = @logTests(q)
		If curPackage <> t->Package Then
			If curPackage <> "" Then c += "</testsuite>" + NL
			c += "<testsuite name=""" + xmlEscape(t->Package) + """>"
			curPackage = t->Package
		EndIf
		
		c += "<testcase classname=""" + xmlEscape(t->Mode + ":" + t->Package + "." + t->Test) + """ name=""" + xmlEscape(t->Test) + """ time=""0"""
		If t->Result = 0 Then
			c += ">" + NL
			c += "<system-out>"
			c += xmlEscape(t->Outp)
			c += "</system-out>" + NL
			c += "</testcase>" + NL
		Else
			c += ">" + NL
			c += "<failure message="""" type=""Failure"">" + NL
			c += xmlEscape(t->Outp)
         c += "</failure>" + NL
			c += "</testcase>" + NL
		EndIf
		
		
	Next
	If curPackage <> "" Then c += "</testsuite>" + NL
	c += "</testsuites>" + NL
	
	If Dir(filename) <> "" Then Kill filename
	Dim ff As Integer = FreeFile
	Open filename For Binary As #ff
	Put #ff,,c
	
	'/
	Return 0
End Function







readTests("cunit-tests.inc")

For q As Integer = 0 To logTestCnt-1
	If checkTest(@logTests(q))	<> 0 Then Print "ERROR processing: " + logTests(q).Package + "." + logTests(q).Test
Next

'writeTestlog("log-tests-Results.xml")


