<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- convert jUnit test results to jUnit test results
         fix test and fail counters and remove unsupported nodes
         merge separate packages with the same name -->
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/testsuites">
        <testsuites><!-- tests="{count(testsuites/testsuite)}" --><!-- disabled="" errors="" failures="" name="" tests="" time="" -->
            <xsl:for-each select="testsuite">
                <xsl:sort select="@name"/>
                <xsl:variable name="name" select="@name"/>
                <xsl:if test="not(preceding-sibling::*[@name = $name])">
                    <testsuite errors="0" name="{@name}" failures="{count(//testsuite[@name=$name]/testcase[./failure])}" skips="0" tests="{count(//testsuite[@name=$name]/testcase)}" time="0">
                        <!--<xsl:apply-templates select="//testsuite[@name=$name]/testcase">
                            <xsl:sort select="@classname" />
                        </xsl:apply-templates>-->

                        <xsl:for-each select=". | following-sibling::*[@name = $name]">
                            <xsl:sort select="@name" />
                            <xsl:apply-templates select="testcase">
                                <xsl:sort select="@name" />
                            </xsl:apply-templates>
                        </xsl:for-each>
                    </testsuite>
                </xsl:if>
            </xsl:for-each>
        </testsuites>
    </xsl:template>

<!--
    <xsl:template match="testsuite">
        <testsuite errors="0" name="{@name}" failures="{count(testcase[./failure])}" skips="0" tests="{count(testcase)}" time="0">
            <xsl:apply-templates select="testcase">
                <xsl:sort select="@name" />
            </xsl:apply-templates>
        </testsuite>
    </xsl:template>
-->

    <xsl:template match="testcase">
        <xsl:copy-of select="."/>
    </xsl:template>

</xsl:transform>