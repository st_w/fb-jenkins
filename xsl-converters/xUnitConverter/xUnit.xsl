<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" version="2.0" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/">
        <testsuites tests="{count(.//CUNIT_RUN_SUITE)}"><!-- disabled="" errors="" failures="" name="" tests="" time="" -->
            <xsl:apply-templates select="CUNIT_TEST_RUN_REPORT/CUNIT_RESULT_LISTING/CUNIT_RUN_SUITE">
                <xsl:sort select=".//SUITE_NAME" />
            </xsl:apply-templates>
            <!--
            <xsl:for-each select="CUNIT_TEST_RUN_REPORT/CUNIT_RESULT_LISTING/CUNIT_RUN_SUITE">
                <xsl:sort select=".//SUITE_NAME"/>
                <xsl:apply-templates/>
            </xsl:for-each>-->
        </testsuites>
    </xsl:template>


    <xsl:template match="CUNIT_RUN_SUITE">
    <testsuite errors="0" name="{.//SUITE_NAME}" failures="{count(.//CUNIT_RUN_TEST_FAILURE)}" skips="0" tests="{count(.//CUNIT_RUN_TEST_RECORD)}" time="0">
        <!--<xsl:variable name="n1" select=".//SUITE_NAME"/>-->
        <!--<xsl:attribute name="name">
            <xsl:value-of select="replace($n1, '^\s+|\s+$', '')"/>
        </xsl:attribute>-->
        <xsl:apply-templates select=".//CUNIT_RUN_TEST_RECORD">
            <xsl:sort select=".//TEST_NAME" />
        </xsl:apply-templates>
    </testsuite>
    </xsl:template>


    <xsl:template match="CUNIT_RUN_TEST_RECORD">
        <testcase classname="{ancestor::CUNIT_RUN_SUITE//SUITE_NAME}.{.//TEST_NAME}" name="{.//TEST_NAME}" time="0" />
    </xsl:template>


</xsl:transform>