
#Ifdef __FB_UNIX__
	#Define NL !"\n"
#Else
	#Define NL !"\r\n"
#EndIf

Dim ff As Integer

Dim errInput As String = Command(1)
Dim errOutput As String = Command(2)

If __FB_ARGC__ <> 3 OrElse errInput = "" OrElse errOutput = "" Then
	Print "Usage: errmsg-update <errorlist> <wakka page>"
	End -1
EndIf

If Dir(errInput) = "" Then
	Print "Error: errorlist input file not found"
	End -1
EndIf

If Dir(errOutput) = "" Then
	Print "Error: wakka page file not found"
	End -1
EndIf



Dim sErrors As String
Dim sWarnings As String
Dim sSection As String Ptr = @sWarnings


Dim ln As String
Dim lastLn As String = ""


' read warning/error messages

ff = FreeFile
Open errInput For Input As #ff

Dim lastNum As Integer
Do Until Eof(ff)
	Line Input #ff, ln
	Dim p As Integer = InStr(ln, "- //")
	If p <= 0 Then lastLn = ln : Continue Do
	p += 4
	Dim p2 As Integer = InStr(p, ln, Any " /")
	Dim num As Integer = Val(Mid(ln, p, p2-p))
	
	If num < lastNum AndAlso Trim(lastLn, Any !" \t\n\r") = "" Then
		sSection = @sErrors
	EndIf
	*sSection += ln + NL
	
	lastNum = num
	lastLn = ln
Loop

Close #ff


' read old wakka page and update warning/error messages

Dim page As String
lastNum = 1000
sSection = @sWarnings
lastLn = ""

ff = FreeFile
Open errOutput For Input As #ff
Do Until Eof(ff)
	Line Input #ff, ln
	Dim p As Integer = InStr(ln, "- //")
	If p <= 0 Then
		page += ln + NL
		lastLn = ln
		Continue Do
	EndIf
	p += 4
	Dim p2 As Integer = InStr(p, ln, Any " /")
	Dim num As Integer = Val(Mid(ln, p, p2-p))
	
	If num < lastNum AndAlso Trim(lastLn, Any !" \t\n\r") = "" Then
		page += *sSection
		sSection = @sErrors
	EndIf
	
	lastNum = num
	lastLn = ln
Loop

Close #ff


' update wakka page

Kill errOutput
ff = FreeFile
Open errOutput For Binary As #ff
Put #ff,,page
Close #ff




