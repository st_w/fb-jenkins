'
' XML
' version 0.7
' (c) 2015 Stefan Wurzinger
'

#ifndef CRLF
	#define CRLF chr(13,10)
#endif

Type XML
	Tag as String
	Flags as UInteger = 0				'Bitmask: 1=Node, 2=Attribute, 4..128=Reserved
	Value as String
	SubTags as String = ""
	Parent as XML ptr
	Declare Function Get(ElemNr as Integer, Flag as UInteger = 0) as XML ptr
	Declare Function Get(Tag as String, ElemNr as Integer = 0, Flag as UInteger = 0) as XML ptr
	Declare Function Count OverLoad (Flag as UInteger = 0) as Integer
	Declare Function Count OverLoad (Tag as String, Flag as UInteger = 0) as Integer
	Declare Function Add(Tag as String = "", Value as String = "", Flags as UInteger = 0) as XML Ptr
	Declare Function Remove OverLoad (ElemNr as Integer, Flag as UInteger = 0) as Integer
	Declare Function Remove OverLoad (Tag as String, ElemNr as Integer = 0, Flag as UInteger = 0) as Integer
	Declare Function Clear(Flag as UInteger = 0) as Integer
	Declare Function SubTagChPos(SubXML as XML ptr, ElemPos as Integer, Flag as UInteger = 0) as Integer
	Declare Function IndexOf(SubXML as XML ptr, Flag as UInteger = 0) as Integer
	Declare Function ChPos(ElemPos as Integer, Flag as UInteger = 0) as Integer
	Declare Function Parse(XMLData as String) as Integer
	Declare Function ToString(Level as Integer = 0) as String
	Declare Destructor()
	
	Enum Flag
		Node = 1
		Attribute = 2
	End Enum
	
	Private:
	Declare Function ParseInternal(XMLData as String, ByRef offset As Integer) as Integer

End Type

Function XML.Parse(XMLData as String) as Integer
	Dim As Integer i
	i = InStr(XMLData, "<?")
	If i > 0 Then
		i = InStr(i+2, XMLData, "?>")
		If i = 0 Then Return -1
	EndIf
	Return This.ParseInternal(XMLData, i)
End Function


Function XML.ParseInternal(XMLData as String, ByRef offset As Integer) as Integer
	Dim q as Integer
	Dim SPos as UInteger
	Dim TPos as UInteger
	Dim EPos as UInteger
	Dim FPos as UInteger
	Dim tmpValue as String
	Dim SepChar as String
	Dim SubXML as XML ptr
	
	If Subtags <> "" then Clear()
	
	this.Tag = ""
	this.Flags = 1
	this.Value = ""
	this.SubTags = ""

	'Parse tagname
	SPos = Instr(offset, XMLData, "<")
	if SPos = 0 then return 0
	While Mid(XMLData, SPos, 4) = "<!--"		'skip comments
		SPos = InStr(SPos+4, XMLData, "-->")
		If SPos = 0 Then Return 22    'missing comment closing tag
		SPos = InStr(SPos+3, XMLData, "<")
		If SPos = 0 Then Return 0
	Wend
	EPos = Instr(SPos+1, XMLData, Any Chr(32, 10, 13))
	TPos = Instr(SPos+1, XMLData, ">")
	if (TPos < EPos and TPos <> 0) or EPos = 0 then EPos = TPos
	' <tagname attr="xyz"/>
	' ^       ^           ^
	' SPos    EPos        TPos
	this.Tag = mid(XMLData, SPos+1, EPos-SPos-1)
	
	'Parse attributes
	if EPos <> TPos then
		if mid(XMLData, TPos-1, 2) = "/>" Then tmpValue = mid(XMLData, EPos+1, TPos-EPos-2) else tmpValue = mid(XMLData, EPos+1, TPos-EPos-1)
		do
			EPos = Instr(tmpValue, "=")
			if EPos = 0 then exit do
			FPos = Instr(EPos+1, tmpValue, "'")
			SPos = Instr(EPos+1, tmpValue, """")
			if (SPos < FPos and SPos <> 0) or FPos = 0 then FPos = SPos
			If FPos = 0 Then Return -1
			SepChar = mid(tmpValue, FPos, 1)
			SPos = Instr(FPos+1, tmpValue, SepChar)
			' <tag attr = "value">
			'           ^ ^     ^
			'        EPos FPos  SPos
			this.Add(trim(left(tmpValue, EPos-1), Any chr(32, 13, 10, 9)), mid(tmpValue, FPos+1, SPos-FPos-1), 2)
			tmpValue = mid(tmpValue, SPos+2)
		Loop
	EndIf
	
	'Parse content
	if mid(XMLData, TPos-1, 2) = "/>" Then
		offset = TPos
		return 0
	EndIf
	Do
		SPos = Instr(TPos+1, XMLData, "<")
		While Mid(XMLData, SPos, 4) = "<!--"		'skip comments
			SPos = InStr(SPos+4, XMLData, "-->")
			If SPos = 0 Then Return 22    'missing comment closing tag
			SPos = InStr(SPos+3, XMLData, "<")
			If SPos = 0 Then Exit Do
		Wend
		If SPos = 0 Then Return 20    'missing (closing) tag
		if Mid(XMLData, SPos+1, 1) = "/" Then
			FPos = InStr(SPos+1, XMLData, this.Tag)
			EPos = InStr(SPos+1, XMLData, ">")
			If FPos = 0 Or FPos > EPos Then Return 21  'unexpected/wrong closing tag
			'no sub tags (except attributes) -> store content in Value
			if len(this.SubTags) = this.Count(2)*SizeOf(Any Ptr) Then
				this.Value = mid(XMLData, TPos+1, SPos-TPos-1)
			EndIf
			offset = EPos
			exit do
		EndIf
		SubXML = New XML
		if SubXML = 0 then return 10		'out of memory
		SubXML->Parent = @this
		this.SubTags &= MKI(cast(UInteger, SubXML))
		offset = SPos
		q = SubXML->ParseInternal(XMLData, offset)
		if q <> 0 then return q
		TPos = offset
	Loop
	
	return 0
End Function

Destructor XML()
	if Subtags <> "" then this.Clear()
End Destructor

Function XML.ToString(Level as Integer = 0) as String
	dim tmpValue as String = ""
	dim SubXML as XML ptr
	tmpValue = String(Level, 9) & "<" & this.Tag
	for q as Integer = 0 to this.Count(2)-1
		tmpValue &= " " & Get(q, 2)->Tag & !"=\"" & Get(q, 2)->Value & !"\""
	Next
	if this.Value <> "" then
		tmpValue &= ">" & this.Value
	else
		if this.Count(1) = 0 then return tmpValue & "/>" & CRLF
		tmpValue &= ">" & CRLF
		for q as Integer = 0 to this.Count(1)-1
			tmpValue &= this.Get(q,1)->ToString(Level+1)
		Next
		tmpValue &= String(Level, 9)
	EndIf
	tmpValue &= "</" & this.Tag & ">" & CRLF
	return tmpValue
End Function

Function XML.Count OverLoad (Flag as UInteger = 0) as Integer
	if Flag = 0 then return len(this.SubTags)/SizeOf(Any Ptr)
	Dim Anz as Integer = 0
	For q as Integer = 0 to len(this.SubTags)/SizeOf(Any Ptr)-1
		if (Cast(XML ptr, CVI(mid(this.SubTags, q*SizeOf(Any Ptr)+1, SizeOf(Any Ptr))))->Flags and Flag) <> 0 then Anz += 1
	Next
	return Anz
End Function

Function XML.Get(ElemNr as Integer, Flag as UInteger = 0) as XML ptr
	if ElemNr >= this.Count(Flag) or ElemNr < 0 then return 0
	if Flag = 0 then return Cast(XML ptr, CVI(mid(this.SubTags, ElemNr*SizeOf(Any Ptr)+1, SizeOf(Any Ptr))))
	Dim Anz as Integer = 0
	Dim SubXML as XML ptr
	For q as Integer = 0 to (len(this.SubTags)/SizeOf(Any Ptr))-1
		SubXML = Cast(XML ptr, CVI(mid(this.SubTags, q*SizeOf(Any Ptr)+1, SizeOf(Any Ptr))))
		if (SubXML->Flags and Flag) <> 0 then Anz += 1
		if ElemNr+1 = Anz then return SubXML
	Next
End Function

Function XML.Get(Tag As String, ElemNr as Integer = 0, Flag as UInteger = 0) as XML ptr
	dim Anz as Integer = 0
	dim SubXML as XML ptr
	for q as Integer = 0 to (len(this.SubTags)/SizeOf(Any Ptr))-1
		SubXML = Cast(XML ptr, CVI(mid(this.SubTags, q*SizeOf(Any Ptr)+1, SizeOf(Any Ptr))))
		if (SubXML->Tag = Tag) and ((Flag = 0) OrElse ((SubXML->Flags and Flag) <> 0)) then Anz += 1
		if ElemNr+1 = Anz then return SubXML
	Next
	return 0
End Function

Function XML.Add(Tag as String = "", Value as String = "", Flags as UInteger = 0) as XML Ptr
	Dim SubXML as XML ptr
	if (Flags and 2) <> 2 then this.Value = ""
	SubXML = New XML
	if SubXML = 0 then return NULL '10
	this.SubTags &= MKI(cast(UInteger, SubXML))
	SubXML->Parent = @this
	SubXML->Tag = Tag
	SubXML->Value = Value
	SubXML->Flags = IIf(Flags = 0, 1, Flags)
	return SubXML
End Function

Function XML.Remove OverLoad (ElemNr as Integer, Flag as UInteger = 0) as Integer
	if ElemNr >= this.Count(Flag) or ElemNr < 0 then return 0
	if Flag = 0 then
		Delete(Cast(XML ptr, CVI(mid(this.SubTags, ElemNr*SizeOf(Any Ptr)+1, SizeOf(Any Ptr)))))
		this.SubTags = left(this.SubTags, ElemNr*SizeOf(Any Ptr)) & mid(this.SubTags, (ElemNr+1)*SizeOf(Any Ptr)+1)
		return 0
	EndIf
	Dim Anz as Integer = 0
	Dim SubXML as XML ptr
	For q as Integer = 0 to (len(this.SubTags)/SizeOf(Any Ptr))-1
		SubXML = Cast(XML ptr, CVI(mid(this.SubTags, q*SizeOf(Any Ptr)+1, SizeOf(Any Ptr))))
		if (SubXML->Flags and Flag) <> 0 then Anz += 1
		if ElemNr+1 = Anz then
			Delete(SubXML)
			this.SubTags = left(this.SubTags, q*SizeOf(Any Ptr)) & mid(this.SubTags, (q+1)*SizeOf(Any Ptr)+1)
			return 0
		EndIf
	Next
	return 0
End Function

Function XML.Remove OverLoad (Tag as String, ElemNr as Integer = 0, Flag as UInteger = 0) as Integer
	if ElemNr >= this.Count(Flag) or ElemNr < 0 then return 0
	Dim Anz as Integer = 0
	Dim SubXML as XML ptr
	For q as Integer = 0 to (len(this.SubTags)/SizeOf(Any Ptr))-1
		SubXML = Cast(XML ptr, CVI(mid(this.SubTags, q*SizeOf(Any Ptr)+1, SizeOf(Any Ptr))))
		if (SubXML->Tag = Tag) and ((Flag = 0) OrElse ((SubXML->Flags and Flag) <> 0)) then Anz += 1
		if ElemNr+1 = Anz then
			Delete(SubXML)
			this.SubTags = left(this.SubTags, q*SizeOf(Any Ptr)) & mid(this.SubTags, (q+1)*SizeOf(Any Ptr)+1)
			return 0
		EndIf
	Next
	return 0
End Function

Function XML.Clear(Flag As UInteger = 0) as Integer
	Dim SubXML as XML ptr
	for q as Integer = (len(this.SubTags)/SizeOf(Any Ptr))-1 to 0 step -1
		SubXML = Cast(XML ptr, CVI(mid(this.SubTags, q*SizeOf(Any Ptr)+1, SizeOf(Any Ptr))))
		if (Flag = 0) OrElse ((SubXML->Flags and Flag) <> 0) then
			Delete(SubXML)
			this.SubTags = left(this.SubTags, q*SizeOf(Any Ptr)) & mid(this.SubTags, (q+1)*SizeOf(Any Ptr)+1)
		EndIf
	Next
	return 0
End Function

Function XML.IndexOf(SubXML as XML ptr, Flag as UInteger = 0) as Integer
	Dim Anz as Integer = 0
	Dim TmpXML as XML ptr
	For q as Integer = 0 to (len(this.SubTags)/SizeOf(Any Ptr))-1
		TmpXML = Cast(XML ptr, CVI(mid(this.SubTags, q*SizeOf(Any Ptr)+1, SizeOf(Any Ptr))))
		if Flag = 0 OrElse (TmpXML->Flags and Flag) <> 0 then
			if TmpXML = SubXML then return Anz
			Anz += 1
		EndIf
	Next
	return -1
End Function

Function XML.SubTagChPos(SubXML as XML ptr, ElemPos as Integer, Flag as UInteger = 0) as Integer
	Dim Anz as Integer = 0
	Dim tmpPos as Integer = ElemPos
	if SubXML = 0 then return 1
	if tmpPos < 0 then tmpPos = 0
	if tmpPos > len(this.SubTags)/4 then tmpPos = len(this.SubTags)/SizeOf(Any Ptr)
	for q as Integer = 0 to len(this.SubTags)/SizeOf(Any Ptr)
		if q = len(this.SubTags)/SizeOf(Any Ptr) then return 15
		if (Cast(XML ptr, CVI(mid(this.SubTags, q*SizeOf(Any Ptr)+1, SizeOf(Any Ptr)))) = SubXML) and (Flag = 0 OrElse ((Cast(XML ptr, CVI(mid(this.SubTags, q*SizeOf(Any Ptr)+1, SizeOf(Any Ptr))))->Flags and Flag) <> 0)) then
			this.SubTags = left(this.SubTags, q*SizeOf(Any Ptr)) & mid(this.SubTags, (q+1)*SizeOf(Any Ptr)+1)
			exit for
		EndIf
	Next
	for q as Integer = 0 to len(this.SubTags)/SizeOf(Any Ptr)
		if ElemPos = Anz then
			this.SubTags = left(this.SubTags, q*SizeOf(Any Ptr)) & MKI(cast(UInteger,SubXML)) & mid(this.SubTags, q*SizeOf(Any Ptr)+1)
			return 0
		EndIf
		if q = len(this.SubTags)/4 then return 1
		if Flag = 0 OrElse (Cast(XML ptr, CVI(mid(this.SubTags, q*SizeOf(Any Ptr)+1, SizeOf(Any Ptr))))->Flags and Flag) <> 0 then Anz += 1
	Next
End Function

Function XML.ChPos(ElemPos as Integer, Flag as UInteger = 0) as Integer
	return this.Parent->SubTagChPos(@this, ElemPos, Flag)
End Function

Function XML.Count OverLoad (Tag as String, Flag as UInteger = 0) as Integer
	Dim Anz as Integer = 0
	Dim SubXML as XML ptr
	For q as Integer = 0 to (len(this.SubTags)/SizeOf(Any Ptr))-1
		SubXML = Cast(XML ptr, CVI(mid(this.SubTags, q*SizeOf(Any Ptr)+1, SizeOf(Any Ptr))))
		if SubXML->Tag = Tag and (Flag = 0 OrElse (SubXML->Flags and Flag) <> 0) then Anz += 1
	Next
	return Anz
End Function

