

#Include "crt.bi"
#Include "xml.bi"
#Include "dir.bi"

Const As String pathSep = "\"
Dim Shared As String fbcPath
Dim Shared As String javacPath
Dim Shared As String workspace

Declare Function pathJoin(pathA As String, pathB As String = "") As String



fbcPath = "C:\Users\swurzi.AUT-SNEU\Downloads\FreeBasic\fbc\fbc.exe"
workspace = "C:\Users\swurzi.AUT-SNEU\Downloads\FreeBasic\fbc-master\examplesW"
If Environ("JAVA_HOME") <> "" Then javacPath = pathJoin(Environ("JAVA_HOME"), "/bin/javac") Else javacPath = "javac"

Dim exampleXML As XML
Dim Shared reqCache As XML

Type StateObject
	basedir As String
	enabled As Boolean
	
End Type


If Open(pathJoin(workspace, "examples.xml") For Binary As #1) Then
	Print "Error while opening 'examples.xml'"
	End
EndIf

Dim fileDat As String = String(Lof(1), 0)
Get #1,1,fileDat
Close #1

Dim As Double a, b

If exampleXML.Parse(fileDat) Then
	Print "Error while parsing 'examples.xml'"
	End
EndIf

If exampleXML.Tag <> "examples" Then
	Print "Error: root element <examples> missing"
	End
EndIf


'Initialize requirements cache
reqCache.Add("lib")
reqCache.Add("compiler")
reqCache.Add("platform")



Function pathJoin(pathA As String, pathB As String = "") As String
	Dim As String path
	If pathB = "" Then
		path = pathA
	ElseIf pathA = "" Then
		path = pathB
	Else
		If Right(pathA,1) = pathSep OrElse Right(pathA,1) = "/" Then
			If Left(pathB,1) = pathSep OrElse Left(pathB,1) = "/" Then
				path = pathA + Mid(pathB,2)
			Else
				path = pathA + pathB
			EndIf
		Else
			If Left(pathB,1) = pathSep OrElse Left(pathB,1) = "/" Then
				path = pathA + pathB
			Else
				path = pathA + pathSep + pathB
			EndIf
		EndIf
	EndIf
	For q As Integer = 0 To Len(path)-1
		If path[q] = Asc("/") Then path[q] = Asc(pathSep)
	Next
	Return path
End Function


Function createTmpFile(ext As String = "", content As String = "") As String
	
	Dim As String tmpFilename = *tmpnam(NULL)
	tmpFilename = pathJoin(workspace, tmpFilename+ext)
	Dim FF As Integer = FreeFile
	Open tmpFilename For Binary As #FF
	Put #FF,,content
	Close #FF
	Return tmpFilename
	
End Function




Function compile (compiler As String = "fbc", files As String, args As String = "", silent As Boolean = True) As Integer
	
#Ifdef __FB_LINUX__
	Const As String nulDevice = "/dev/null"
#Else
	Const As String nulDevice = "NUL"
#EndIf
	
	Dim As String cmd
	Select Case compiler
		Case "fbc" : cmd = """" + pathJoin(fbcPath)+ """ " + args + " " + files 
		Case "javac": cmd = """" + pathJoin(javacPath)+ """ " + args + " " + files
		Case "custom": cmd = files + " " + args
		Case Else: cmd = """" + pathJoin(compiler)+ """ " + args + " " + files 
	End Select
	
	If silent Then
		Return Shell("""" + cmd + " > " + nulDevice + " 2>&1" + """")
	Else
		Return Shell("""" + cmd + """")
	EndIf
End Function


Function processRequire(root As XML Ptr, invert As boolean = FALSE) As Boolean
	
	Dim As XML Ptr node
	Dim cacheNode As XML Ptr = 0
	
	Dim globalResult As Boolean = TRUE
	
	For q As Integer = 0 To root->Count(XML.Attribute)
		node = root->Get(q, XML.Attribute)
		
		Dim result As Boolean = FALSE
		
		Dim tmpNode As XML Ptr = reqCache.Get(node->Tag)
		If tmpNode = 0 Then Return FALSE
		cacheNode = tmpNode->Get(node->Value)
		For q As Integer = 0 To tmpNode->Count()
			If tmpNode->Get(q)->Tag = node->Value Then
				cacheNode = tmpNode->Get(q)
				result = cbool(cacheNode->Value)
				Exit For
			EndIf
		Next
		
		If cacheNode <> 0 Then
			
			Print "Checking for requirement: " + node->Tag + " '" + node->Value + "' ... ";
			
		ElseIf node->Tag = "lib" Then
			
			Print "Checking for requirement: lib '" + node->Value + "' ... ";
			Dim As String tmpF = createTmpFile(".bas")
			result =  (compile(, tmpF, "-l " + node->Value + " -x """ + tmpF + ".out""") = 0)
			Kill tmpF
			Kill tmpF + ".out"
		
		ElseIf node->Tag = "compiler" Then
			
			Print "Checking for requirement: compiler '" + node->Value + "' ... ";
			
			Select Case node->Value
				
				Case "fbc":
					Dim As String tmpF = createTmpFile(".bas")
					result = (compile(, tmpF, "-x """ + tmpF + ".out""") = 0)
					Kill tmpF
					Kill tmpF + ".out"
					
				Case "gcc", "g++":
					Dim As String tmpF = createTmpFile(".c", "int main() {}")
					result = (compile(node->Value, tmpF, "-o """ + tmpF + ".out""", false) = 0)
					Kill tmpF
					Kill tmpF + ".out"
					
				Case "javac":
					Dim As String tmpF = createTmpFile(".java")
					result = (compile("javac", tmpF) = 0)
					Kill tmpF
					Kill Left(tmpF, Len(tmpF)-5) + ".class"
				
			End Select
			
		ElseIf node->Tag = "platform" Then
		
			Select Case LCase(node->Value)
				
				Case "dos":
					#Ifdef __FB_DOS__
						result = TRUE
					#EndIf
				Case "windows"
					#Ifdef __FB_WIN32__
						result = TRUE
					#EndIf
				Case "linux"
					#Ifdef __FB_LINUX__
						result = TRUE
					#EndIf
				Case "unix"
					#Ifdef __FB_UNIX__
						result = TRUE
					#EndIf
				Case "freebsd"
					#Ifdef __FB_FREEBSD__
						result = TRUE
					#EndIf
				Case "darwin"
					#Ifdef __FB_DARWIN__
						result = TRUE
					#EndIf
				Case "netbsd"
					#Ifdef __FB_NETBSD__
						result = TRUE
					#EndIf
				Case "openbsd"
					#Ifdef __FB_OPENBSD__
						result = TRUE
					#EndIf
				Case "cyqwin"
					#Ifdef __FB_CYGWIN__
						result = TRUE
					#EndIf
				Case "pcos"
					#Ifdef __FB_PCOS__
						result = TRUE
					#EndIf
					
			End Select
			
		ElseIf node->Tag = "architecture" Then
		
			Select Case LCase(node->Value)
				
				Case "x86":
					#If Not Defined(__FB_64BIT__) And Not Defined(__FB_ARM__)
						result = TRUE
					#EndIf
				Case "x64", "x86-64":
					#If Defined(__FB_64BIT__) And Not Defined(__FB_ARM__)
						result = TRUE
					#EndIf
				Case "arm":
					#Ifdef __FB_ARM__
						result = TRUE
					#EndIf
				
			End Select
			
			
		EndIf
		
		If cacheNode <> 0 Then
			If result Then	Print "OK (cached)" Else Print "Fail (cached)"
		Else
			If result Then	Print "OK" Else Print "Fail"
			cacheNode = reqCache.Get(node->Tag)
			cacheNode->Add(node->Value, Str(result))
		EndIf
		
		If invert Then
			If result Then globalResult = FALSE: Exit For
		Else
			If Not result Then globalResult = FALSE: Exit For
		EndIf
		
	Next
	
	Return globalResult
End Function


Sub walkDir(basedir As String, pattern As String, oList As XML Ptr, recurse As Boolean = FALSE)
	Dim As Integer Ptr dirState
	Dim f As String
	
	f = Dir(pathJoin(basedir),fbDirectory,dirState)
	While f <> ""
		If f <> "." AndAlso f <> ".." Then
			walkDir(pathJoin(basedir, f), pattern, oList, recurse)
		EndIf
		f = Dir(dirState)
	Wend
	
	f = Dir(pathJoin(basedir, pattern),,dirState)
	While f <> ""
		oList->Add(pathJoin(basedir, f))
		
		f = Dir(dirState)
	Wend
	
End Sub



Function processCompile(node As XML Ptr, ByRef state As StateObject, cList As XML Ptr) As Integer
	If node->Tag = "compile" Then
		Dim As String sources
		Dim As String options
		Dim As String compiler
		Dim As XML Ptr tmpNode
		
		tmpNode = node->Get("options")
		If tmpNode <> 0 Then options += " " + tmpNode->Value
		For w As Integer = 0 To node->count("source")
			tmpNode = node->Get("source", w)
			Dim As String tmpSrc
			If tmpNode->Get("file") <> 0 Then
				tmpSrc = pathJoin(state.basedir, tmpNode->Get("file")->Value)
			Else
				tmpSrc = pathJoin(state.basedir, tmpNode->Value)
			EndIf
			sources += " """ + tmpSrc  + """"
			If cList <> 0 Then cList->Add(tmpSrc)
		Next
		tmpNode = node->Get("compiler")
		If tmpNode <> 0 Then compiler = tmpNode->Value
		
		If state.enabled Then
			compile(compiler, sources, options)
		EndIf
	
	ElseIf node->Tag = "skip" Then
		For w As Integer = 0 To node->count("file")
			Dim As XML Ptr tmpNode = node->Get("file", w)
			If cList <> 0 Then cList->Add(pathJoin(state.basedir, tmpNode->Value))
		Next
	
	EndIf
	
	
End Function


Function processExample(root As XML Ptr, ByVal state As StateObject) As Integer
	
	Dim As XML Ptr node
	Dim globalOpt As String
	
	Dim cList As XML
	
	For q As Integer = 0 To root->Count(XML.Attribute)
		node = root->Get(q, XML.Attribute)
		
		If node->Tag = "basedir" Then
			state.basedir = pathJoin(state.basedir, node->Value)
			ChDir(pathJoin(workspace, state.basedir))
			
		ElseIf node->Tag = "options" Then
			globalOpt = node->Value
			
		ElseIf node->Tag = "source" Then
			root->Add("compile")->Add("source", node->Value)
			
		EndIf
	Next
	
	For q As Integer = 0 To root->Count(XML.Node)
		node = root->Get(q, XML.Node)
		Select Case node->Tag
			Case "require": state.enabled = state.enabled AndAlso processRequire(node)
			Case "requireNot": state.enabled = state.enabled AndAlso processRequire(node, TRUE)
		End Select
	Next
	
	For q As Integer = 0 To root->Count(XML.Node)
		node = root->Get(q, XML.Node)
		If node->Tag = "compile" OrElse node->Tag = "skip" Then
			processCompile(node, state, @cList)
		EndIf
	Next
	
	For q As Integer = 0 To root->Count(XML.Node)
		node = root->Get(q, XML.Node)
		If node->Tag = "compileList" Then
			
		EndIf
	Next
		

	
	'compile(, """"+node->Value+"""", globalOpt
	
	
	Return 0
End Function


Function processExampleList(root As XML Ptr, ByVal state As StateObject) As Integer
	
End Function



Function processExamples(root As XML Ptr, ByVal state As StateObject) As Integer
	
	Dim As XML Ptr node
	
	For q As Integer = 0 To root->Count(XML.Attribute)
		node = root->Get(q, XML.Attribute)
		
		If node->Tag = "basedir" Then
			state.basedir = pathJoin(state.basedir, node->Value)
			ChDir(pathJoin(workspace, state.basedir))
		EndIf
	Next
	
	For q As Integer = 0 To root->Count(XML.Node)
		node = root->Get(q, XML.Node)
		Select Case node->Tag
			Case "require": state.enabled = state.enabled AndAlso processRequire(node)
			Case "requireNot": state.enabled = state.enabled AndAlso processRequire(node, TRUE)
		End Select
	Next
	
	For q As Integer = 0 To root->Count(XML.Node)
		node = root->Get(q, XML.Node)
		Select Case node->Tag
			
			Case "example": processExample(node, state)
			Case "examples": processExamples(node, state)
			Case "exampleList": processExampleList(node, state)
			
		End Select
	Next
	
	Return 0
End Function


Dim initialState As StateObject
initialState.basedir = ""
initialState.enabled = TRUE

processExamples(@exampleXML, initialState)





Print
Print "Ready."
Sleep



